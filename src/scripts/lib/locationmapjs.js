
(function( $ ) {
	$.fn.AswoLocationMap = function( options ) {
		
		// Default settings ---------------------------------------------------------------
		
		var settings = $.extend( {
		}, options);

		
		// Plugin properties ---------------------------------------------------------------
		
		var base = this;
		var mainContainer = $(this);
		var mapContainer = mainContainer.find('#tx-aswo-locationmap-countriesmap');
        var tooltip = mainContainer.find('#tx-aswo-locationmap-countriesmap-tooltip');
        var countryInfoContainer = mainContainer.find('#tx-aswo-locationmap-countryinfo');
        var countrySelector = mainContainer.find('#tx-aswo-locationmap-countryselector');
        var countryNameContainer = mainContainer.find('#tx-aswo-locationmap-countryinfo-countryname');


		
		// Plugin methods ---------------------------------------------------------------


        // Select country

        this.selectCountry = function(id){
            var pathToSelect = mapContainer.find('path[data-uid="'+id+'"]');
            var countryName = pathToSelect.attr('data-name');
            countryNameContainer.text(countryName);
            mapContainer.find('path[data-selected="1"]').each(function(index) {
                $(this).attr('data-selected',0);
                $(this).attr('fill',$(this).attr('data-fill-default'));
            });
            pathToSelect.attr('data-selected',1);
            pathToSelect.attr('fill',pathToSelect.attr('data-fill-selected'));
            pathToSelect.attr('fill-opacity',1);
        };

        // Display location

        this.displayLocation = function(id){
            countryInfoContainer.find('.tx-aswo-locationmap-locationdata').hide();
            countryInfoContainer.find('div[data-location-uid="'+id+'"]').show();
            countryInfoContainer.show();
        };

		// Main logic init -------------------------------------------------------------------		

		
		// Events --------------------------------------------------------------------

        mapContainer.find('path').on('click', function(e) {
        	e.preventDefault();
            if($(this).attr('data-haslocation') == '1') {
                var countryUid = $(this).attr('data-uid');
                var locationUid = $(this).attr('data-location-uid');
                base.selectCountry(countryUid);
                base.displayLocation(locationUid);
            }
        });

        mapContainer.find('path').on('mouseover', function(e) {
        	var pathPosition = $(this).offset();
            var parentPosition = $(this).parents('#tx-aswo-locationmap-countriesmap').offset();
            var tooltipText = $(this).attr('data-tooltip');
            var tooltipTop = parseInt(pathPosition.top - parentPosition.top + 250 );
            var tooltipLeft = parseInt(pathPosition.left - parentPosition.left);
            tooltip.find('span').text(tooltipText);
            tooltip.css({ top: tooltipTop , left: tooltipLeft }).show();
            if($(this).attr('data-haslocation') == '1') {
                $(this).attr('fill-opacity',$(this).attr('data-fill-hoveropacity'));
            }
        });

        mapContainer.find('path').on('mouseout', function(e) {
            tooltip.hide();
            if($(this).attr('data-selected') == '0') {
                $(this).attr('fill-opacity',1);
            }
        });

        countrySelector.on('change', function() {
            var countryUid = $(this).val();
            var locationUid = $(this).find(':selected').data('location-uid');
            base.selectCountry(countryUid);
            base.displayLocation(locationUid);
        });
	
		return this;
	};
})( jQuery );

