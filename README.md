# ASWO - 06.05.2019 - v1.01

**Production files and folders for html / css templates**
Author: [Emmanuel Mutel](http://www.sanpan.com)

## Install (on windows)

### Required
Install node.js (and npm): [Node.js](https://nodejs.org/en/)
Functionnal with:

- node 8.9.1 or 10.15.3
- npm 6.9.0
- bower 1.8.8
- Gulp 3.9.1 

Install Git: https://git-scm.com/downloads

- Git 2.21.0.windows.1


### Process
Open command line tool and write followings instructions:

- [1]: npm install npm@6.9.0 -g
- [2]: npm install --global gulp-cli
- [3]: npm install
- [4]: npm install -g bower
- [5]: bower install --save bootstrap-sass#3.3.7
- [6]: gulp build
- [7]: gulp prod

## Gulpfile tasks

- [css] to compile scss files from 'src/scss' folder to css files in 'dest/css' folder.
- [minify] to minify app.css in 'dest/css' folder.
- [browserSync] reload index.html in browser.
- [html] To publish html files from 'src' folder to 'dist' folder.
- [scripts] concatenate and minify all scripts (jquery + bootstrap + scripts in the 'scripts/lib' folder) to a main.js file in the 'dest/scripts' folder.
- [images] publish image files from the 'src/img' folder to the 'dist/img' folder.
- [fonts] publish fonts files from the 'src/css/fonts' folder to the 'dist/css/fonts' folder.
- [clean] Delete the 'dist' folder.
- [default] Tasks [css] + [fonts]
- [build] Task [css]
- [prod] Tasks [build] + [minify] + [fonts] + [images] + [html] + [scripts] + [menu-js]: the complete flow for production, generates all files and folders in the 'dist' folder.
 
- [watch] During the development: monitoring of the files (html, css, js) being modified and automatic update of the preview in the browser.


## Folders structure
!! After a gulp prod task, the folder structure is:

|_ bower_components
|_ dist
    |_ css
    |_ Images
    |_ scripts
|_ node_modules
|_ src
    |_ css
    |_ Images
    |_ scripts
    |_ scss


## SCSS files
- app.scss is the main file, compiled into app.min.css (the css used on the website). It imports all other scss and css.
- variable-custom.css: used to customise bootstrap 3.3.7 variables default (colors, ... )
- boostrap-custom: used to customise bootstrap scss calls
- font-face.css: calls to webfont used in templates (locals paths has been modified after site production)
- includemedia.scss: usefull media queries in Sass (see doc in scss files)
- variable-aswo.css: additional variables for aswo
- aswo-icon.scss: specific font for aswo icons 


## Html templates
!! Depreciated: changes in webfont calls can create display issues (icons and texts)

- index.html (home)
- index.intl.html (home for each country)
- inside.html
- inside2.html
- inside3.html
- localisation.html
- opportunities.html
- search-results.html
- history.html
