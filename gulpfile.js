/****************************************/
/*********   SETUP GULP FILE     ********/
/****************************************/
// @AUTHOR : Emmanuel Mutel

// Open commande line and tape followings instructions :

// [1] : npm install npm@6.9.0 -g
// [2] : npm install --global gulp-cli
// [3] : npm install
// [4] : npm install -g bower
// [5] : bower install --save bootstrap-sass#3.3.7
// [6] : gulp build
// [7] : gulp prod

/****************************************/

//js gulpfile.js
var gulp = require('gulp');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var cleanCSS = require ('gulp-clean-css');

//  Requires the browsersync plugin
var browserSync = require('browser-sync');

// Include plugins
var plugins = require('gulp-load-plugins')(); // all plugins from package.json

// plugin delete
var del = require('del');

//Synchro Browser
gulp.task('browserSync', function () {
	browserSync({
		server: {
			baseDir: config.publicDir
		}
	});
});

// Paths
var config = {
	bower: './bower_components',
	bootstrapDir: './bower_components/bootstrap-sass',
	publicDir: './dist'
};

// sass compile 
gulp.task('css', function () {
	return gulp.src('./src/scss/app.scss')
		.pipe(sass({
			includePaths: [config.bootstrapDir + '/assets/stylesheets'],
			outputStyle: 'nested'
		}).on('error', plugins.sass.logError))
		.pipe(plugins.csscomb())
		.pipe(plugins.cssbeautify({
			indent: '  '
		}))
		.pipe(plugins.autoprefixer('last 3 versions'))
		.pipe(gulp.dest(config.publicDir + '/css'))
		.pipe(plugins.notify({
			message: '♠ Styles task complete'
		}))
		.pipe(browserSync.reload({
			stream: true
		}));
});


// Minify task = minification CSS (dist folder -> dist folder)
gulp.task('minify', function () {
	return gulp.src(config.publicDir + '/css/app.css')
		.pipe(cleanCSS())
		.pipe(plugins.rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(config.publicDir + '/css'))
		.pipe(plugins.notify({
			message: '♠ Minify task complete'
		}));

});

// publish html templates task
gulp.task('html', function () {
	return gulp.src('src/*.html')
		.pipe(gulp.dest(config.publicDir))
		.pipe(plugins.notify({
			message: '♠ html task complete'
		}));

});


// javascript task
gulp.task('scripts', function () {
	return gulp.src([config.bower + '/jquery/dist/jquery.js', config.bower + '/bootstrap-sass/assets/javascripts/bootstrap.js', 'src/scripts/lib/*.js'])
		.pipe(gulp.dest(config.publicDir + '/scripts/'))
		.pipe(plugins.notify({
			message: '♠ scripts task complete'
		}))
		.on('end', function () {
			gutil.log(gutil.colors.yellow('♠ La tâche SCRIPTS est terminée.'));
		});
});


// Images task
gulp.task('images', function () {
	return gulp.src('src/Images/**/*')
		.pipe(plugins.cache(plugins.imagemin({
			optimizationLevel: 3,
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest(config.publicDir + '/Images'))
		.pipe(plugins.notify({
			message: '♠ Images task complete'
		}));
});

gulp.task('fonts', function () {
	return gulp.src([config.bootstrapDir + '/assets/fonts/**/*', './src/css/fonts/**/*'])
		.pipe(gulp.dest(config.publicDir + '/css/fonts'));
});

// Clean task
gulp.task('clean', function () {
	return del([config.publicDir + '/css', config.publicDir + '/scripts', config.publicDir + '/Images']);
});

// Default task
gulp.task('default', ['css', 'fonts']);

// Build task
gulp.task('build', ['css']);

// Prod task = Build + minify + fonts + images + html + scripts
gulp.task('prod', [ 'build', 'fonts', 'images', 'html', 'scripts', 'minify']);

//watch alive dev
gulp.task('watch', ['scripts', 'browserSync', 'css', 'minify'], function () {
	gulp.watch('src/scripts/lib/*.js', ['scripts']);
	gulp.watch('src/scss/**/*.scss', ['css']);
	gulp.watch('dist/css/app.css', ['minify']);
	// Reloads the browser whenever HTML,css or JS files change
	gulp.watch('src/*.html', ['html']);
	gulp.watch('src/*.html', browserSync.reload);
	gulp.watch('js/**/*.js', browserSync.reload);
});
